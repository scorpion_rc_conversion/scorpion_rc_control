/**
 * \file   pi_microcontroller_interface.h
 * \brief  Brief description of file.
 * \Author James D. Burton (james.burton@vt.edu)
 * \date   September, 2016
 *
 * Interface between PI and Scorpion RC Car microcontroller.
 */

#ifndef __Car_MICROCONTROLLER_INTERFACE__
#define __Car_MICROCONTROLLER_INTERFACE__

#include <wiringPi.h>

#include <scorpion_rc_msgs/scorpion_rc_types.h>

namespace scorpion_rc
{

class CarMicrocontrollerInterface
{
public:
	/*
		Default Constructor
	*/
	CarMicrocontrollerInterface();

	/*
		Constructor
	*/
	CarMicrocontrollerInterface(const CarFunctionGPIOMap& pin_map, const CarFunctionMap& function_map, const double max_vel);

	/*
		Deconstructor

		Resets GPIO pins to LOW.
	*/
	~CarMicrocontrollerInterface();

	/*
		Update function

		Update pin states according to most recent active function map.

		This should be called by the controller update function.
	*/
	void update(const CarFunctionMap& function_map, const double vel);


protected:
	/*
		Initializer function that setups GPIO pins.
	*/
	void init();

	/*
		Reset GPIO pins to a 'safe' state.
	*/
	void cleanUpGPIO();

	/*
		Simple linear interpolation function to convert a given velocity to a PWM value.
	*/
	int velToPwm(const double vel);

	const int _max_pwm;
	const double _max_vel;
	const CarFunctionGPIOMap _gpio_map;
};

} // end namespace scorpion_rc

#endif
