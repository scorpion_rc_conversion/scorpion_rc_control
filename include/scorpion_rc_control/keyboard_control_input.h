/**
 * \file   keyboard_control_input.h
 * \brief  Brief description of file.
 * \Author James D. Burton (james.burton@vt.edu)
 * \date   September, 2016
 *
 * Keyboard interface for the Scorpion RC Car. This class uses Ncurses to accept
 * keyboard control inputs and to display active car functions.
 */


#ifndef __KEYBOARD_CONTROL__
#define __KEYBOARD_CONTROL__

#include <ncurses.h>

// Ignore shadowing issues in ROS
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <ackermann_msgs/AckermannDriveStamped.h>
#include <scorpion_rc_msgs/CarTelemetry.h>
#pragma GCC diagnostic pop

#include <scorpion_rc_msgs/scorpion_rc_types.h>

namespace scorpion_rc
{

class KeyboardControlInput
{

public:
  KeyboardControlInput(ros::NodeHandle& nh);
  // ~KeyboardControlInput() = default;
  ~KeyboardControlInput();

protected:
  void UpdateLoop();

  void ProcessInput(const int& c);

  void PublishCommands();

  void InitDisplay();

  void PrintDisplay();

  void TelemetryCallback(const scorpion_rc_msgs::CarTelemetry msg);


  ros::NodeHandle _nh;

  // Screen size
  size_t _row;
  size_t _col;

  // ROS Publishers
  ros::Publisher _turn_publisher;
  ros::Publisher _forward_publisher;
  ros::Publisher _turbo_publisher;
  ros::Publisher _backward_publisher;
  ros::Publisher _f1_publisher;
  ros::Publisher _f2_publisher;

  // ROS Subscribers
  ros::Subscriber _telemetry_subscriber;

  CarFunctionMap _active_functions; // Active functions as reported by telemetry
  CarFunctionMap _cmd_functions; // Desired active functions given user input
};

} // end namespace scorpion_rc

#endif
