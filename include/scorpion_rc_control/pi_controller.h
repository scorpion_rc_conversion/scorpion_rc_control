/**
 * \file   pi_controller.h
 * \brief  Scorpion RC Car controller for the Raspberry Pi3.
 * \Author James D. Burton (james.burton@vt.edu)
 * \date   September, 2016
 *
 * Scorpion RC Car controller for the Raspberry Pi3.
 *
 * The controller defined by this header file preforms the following functions
 *
 * - Receives ROS messages specifying car function commands
 * - Interfaces with the integrated microcontroller on the RC car
 * - TODO: Performs basic PID control of car velocity using encoder feedback
 * - TODO: Performs software PWM of steering commands
 * - Publishes controller and (TODO) encoder telemetry data
 */

#ifndef __PI_CONTROLLER__
#define __PI_CONTROLLER__

#include <map>
#include <mutex>
#include <vector>
#include <thread>

// Ignore shadowing issues in ROS
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"
#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <ackermann_msgs/AckermannDriveStamped.h>
#include <scorpion_rc_msgs/CarTelemetry.h>
#pragma GCC diagnostic pop

#include <scorpion_rc_msgs/scorpion_rc_types.h>
#include <scorpion_rc_control/car_microcontroller_interface.h>

namespace scorpion_rc
{

class PiController
{
public:
  /*
    Class Constructor

    nh: Reference to ros::NodeHanlde
  */
  PiController(ros::NodeHandle& nh);

  /*
    Class Destructor

    The destructor waits for worker threads to join.
  */
  ~PiController();

  /*
    Initialization function

    Run by the constructor, this function gets configuration data from the
    ROS parameter server and initializes the controller.
  */
  void init();

  /*
    Controller update function

    Run during each iteration of the control loop, this function updates
    interfaces and the ROS message queues.
  */
  void update();

  /*
    Publisher loop

    Run in a separate thread, this function publish telemetry data to ROS.
  */
  void publishLoop();

  void PublishTelemetry();

  // ROS Message Callbacks
  void TurnCallback(const ackermann_msgs::AckermannDriveStamped msg);
  void ForwardCallback(const std_msgs::Bool msg);
  void TurboCallback(const std_msgs::Bool msg);
  void BackwardCallback(const std_msgs::Bool msg);
  void F1Callback(const std_msgs::Bool msg);
  void F2Callback(const std_msgs::Bool msg);


protected:
  ros::NodeHandle _nh;
  ros::Rate _update_rate;
  ros::Rate _telemetry_rate;

  scorpion_rc::CarFunctionMap _active_functions;
  scorpion_rc::CarFunctionGPIOMap _function_pin_map;

  std::shared_ptr<std::thread> _publishing_thread_prt;
  std::shared_ptr<scorpion_rc::CarMicrocontrollerInterface> _microcontroller_interface_ptr;

  size_t _loop_count;

  // ROS publishers
  ros::Publisher _telemetry_publisher;

  // ROS subscribers
  ros::Subscriber _turn_subscriber;
  ros::Subscriber _forward_subscriber;
  ros::Subscriber _turbo_subscriber;
  ros::Subscriber _backward_subscriber;
  ros::Subscriber _f1_subscriber;
  ros::Subscriber _f2_subscriber;

  double _desired_turn_angle;
  double _desired_velocity;

  std::mutex _mutex;


};

} // end namespace scorpion_rc

#endif
