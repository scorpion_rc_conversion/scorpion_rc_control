/**
 * \file   pi_microcontroller_interface.cpp
 * \brief  Brief description of file.
 * \Author James D. Burton (james.burton@vt.edu)
 * \date   September, 2016
 *
 * Detailed description of file.
 */

#include "scorpion_rc_control/car_microcontroller_interface.h"

namespace scorpion_rc
{

// public
/*
	Default Constructor
*/
CarMicrocontrollerInterface::CarMicrocontrollerInterface():
	_max_pwm(0), // The PWM goes from 0-1024.
	_max_vel(0),
	_gpio_map()
{
}


/*
	Constructor
*/
CarMicrocontrollerInterface::CarMicrocontrollerInterface(const CarFunctionGPIOMap& pin_map, const CarFunctionMap& function_map, const double max_vel):
	_max_pwm(1024), // The PWM goes from 0-1024.
	_max_vel(max_vel),
	_gpio_map(pin_map)
{
	init();
}

CarMicrocontrollerInterface::~CarMicrocontrollerInterface()
{
	cleanUpGPIO();
}

/*
	Update function. Update pin states according to most recent active function map.
*/
void CarMicrocontrollerInterface::update(const CarFunctionMap& function_map, const double vel)
{
	for(auto ii = function_map.begin(); ii != function_map.end(); ++ii)
	{
		if(ii->first == scorpion_rc::CarFunction::FORWARD)
		{
			pwmWrite(_gpio_map.at(ii->first), velToPwm(vel));
		}
		else
		{
			digitalWrite(_gpio_map.at(ii->first), ii->second);
		}
	}
}


// protected
/*
	Initializer function. Setup GPIO pins.
*/
void CarMicrocontrollerInterface::init()
{
	wiringPiSetup();

	for(auto ii = _gpio_map.begin(); ii != _gpio_map.end(); ++ii)
	{
		// The Pi3 only has one PWM output pin. TODO: Create software PWM for other pins.
		if(ii->first == scorpion_rc::CarFunction::FORWARD)
		{
			pinMode(ii->second, PWM_OUTPUT);
			pullUpDnControl(ii->second, PUD_DOWN);
			pwmWrite(ii->second, 0);			}
		else
		{
			pinMode(ii->second, OUTPUT);
			pullUpDnControl(ii->second, PUD_DOWN);
			digitalWrite(ii->second, LOW);
		}
	}
}

/*
	Reset GPIO pins to a 'safe' state if necessary
*/
void CarMicrocontrollerInterface::cleanUpGPIO()
{
	for(auto ii = _gpio_map.begin(); ii != _gpio_map.end(); ++ii)
	{
		digitalWrite(ii->second, LOW);
	}
}

int CarMicrocontrollerInterface::velToPwm(const double vel)
{
	return static_cast<int>(_max_pwm * (vel / _max_vel));
}

} // end namespace scorpion_rc
