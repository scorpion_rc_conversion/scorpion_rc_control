/**
 * \file   keyboard_control_input.cpp
 * \brief  Brief description of file.
 * \Author James D. Burton (james.burton@vt.edu)
 * \date   September, 2016
 *
 * Implementation of keyboard interface for the Scorpion RC Car.
 */

#include "scorpion_rc_control/keyboard_control_input.h"

namespace scorpion_rc
{
// Public
  KeyboardControlInput::KeyboardControlInput(ros::NodeHandle& nh):
  _nh(nh),
  _row(25), // Default terminal size. Overwritten during initialization.
  _col(80),
  _turn_publisher(),
  _forward_publisher(),
  _turbo_publisher(),
  _backward_publisher(),
  _f1_publisher(),
  _f2_publisher(),
  _telemetry_subscriber(),
  _active_functions(),
  _cmd_functions()
  {
    // Initialize function maps
    _cmd_functions = {
      {CarFunction::LEFT, false},
      {CarFunction::RIGHT, false},
      {CarFunction::FORWARD, false},
      {CarFunction::TURBO, false},
      {CarFunction::BACK, false},
      {CarFunction::F1, false},
      {CarFunction::F2, false}
    };

    _active_functions = _cmd_functions;


    _turn_publisher = _nh.advertise<ackermann_msgs::AckermannDriveStamped>("/scorpion_rc/turn", 10);
    _forward_publisher = _nh.advertise<std_msgs::Bool>("/scorpion_rc/forward", 10);
    _turbo_publisher = _nh.advertise<std_msgs::Bool>("/scorpion_rc/turbo", 10);
    _backward_publisher = _nh.advertise<std_msgs::Bool>("/scorpion_rc/back", 10);
    _f1_publisher = _nh.advertise<std_msgs::Bool>("/scorpion_rc/f1", 10);
    _f2_publisher = _nh.advertise<std_msgs::Bool>("/scorpion_rc/f2", 10);

    _telemetry_subscriber = _nh.subscribe("/scorpion_rc/telemetry", 10, &KeyboardControlInput::TelemetryCallback, this);


    InitDisplay();

    UpdateLoop();
  }

  KeyboardControlInput::~KeyboardControlInput()
  {
    endwin();
  }

// Protected
  void KeyboardControlInput::UpdateLoop()
  {
    ros::Rate update_rate(20); // 20 Hz

    while(ros::ok())
    {
      mvprintw(15, 30, "%i", ros::ok());

      int c = getch();

      PrintDisplay();

      mvprintw(15, 0, "KEY NAME : %s - %d\n", keyname(c), c);

      ProcessInput(c);

      refresh();


      PublishCommands();

      ros::spinOnce();
      update_rate.sleep();
    }
  }

  void KeyboardControlInput::ProcessInput(const int& c)
  {
    switch(c)
    {
      case KEY_UP:
        mvprintw(15, 40, "Up pressed");

        if(_cmd_functions[CarFunction::BACK])
        {
          _cmd_functions[CarFunction::BACK] = false;
        }
        else
        {
          _cmd_functions[CarFunction::FORWARD] = true;
        }

        // Print arrow in red
        attron(COLOR_PAIR(1));
        attron(A_BOLD);
        mvprintw(8, (_col*3/4)-1, "/\\");
        mvprintw(9, (_col*3/4)-1, "||");
        attroff(COLOR_PAIR(1));
        attroff(A_BOLD);

        break;
      case KEY_DOWN:
        mvprintw(15, 40, "Down pressed");

        if(_cmd_functions[CarFunction::FORWARD])
        {
          _cmd_functions[CarFunction::FORWARD] = false;
        }
        else
        {
          _cmd_functions[CarFunction::BACK] = true;
        }

        attron(COLOR_PAIR(1));
        attron(A_BOLD);
        mvprintw(11, (_col*3/4)-1, "||");
        mvprintw(12, (_col*3/4)-1, "\\/");
        attroff(COLOR_PAIR(1));
        attroff(A_BOLD);

        break;
      case KEY_RIGHT:
        mvprintw(15, 40, "Right pressed");

        if(_cmd_functions[CarFunction::LEFT])
        {
          _cmd_functions[CarFunction::LEFT] = false;
        }
        else
        {
          _cmd_functions[CarFunction::RIGHT] = true;
        }

        attron(COLOR_PAIR(1));
        attron(A_BOLD);
        mvprintw(10, (_col*3/4)+1, "==>");
        attroff(COLOR_PAIR(1));
        attroff(A_BOLD);

        break;
      case KEY_LEFT:
        mvprintw(15, 40, "Left pressed");

        if(_cmd_functions[CarFunction::RIGHT])
        {
          _cmd_functions[CarFunction::RIGHT] = false;
        }
        else
        {
          _cmd_functions[CarFunction::LEFT] = true;
        }

        attron(COLOR_PAIR(1));
        attron(A_BOLD);
        mvprintw(10, (_col*3/4)-4, "<==");
        attroff(COLOR_PAIR(1));
        attroff(A_BOLD);

        break;
      case 't':
        mvprintw(15, 40, "t pressed");

        _cmd_functions[CarFunction::TURBO] = !_cmd_functions[CarFunction::TURBO];

        break;
      case 'q':
        mvprintw(15, 40, "q pressed");

        ros::shutdown();

        break;
    }
  }

  void KeyboardControlInput::PublishCommands()
  {
    ackermann_msgs::AckermannDriveStamped turn_msg;
    std_msgs::Bool forward_msg;
    std_msgs::Bool back_msg;
    std_msgs::Bool turbo_msg;
    // std_msgs::Bool f1_msg; // Unused at the moment
    // std_msgs::Bool f2_msg;

    turn_msg.header.stamp = ros::Time::now();

    if(_cmd_functions[CarFunction::RIGHT])
    {
      turn_msg.drive.steering_angle = -1; // This currently only indicates direction
    }
    else if(_cmd_functions[CarFunction::LEFT])
    {
      turn_msg.drive.steering_angle = 1;
    }
    else
    {
      turn_msg.drive.steering_angle = 0;
    }

    forward_msg.data = false;
    if(_cmd_functions[CarFunction::FORWARD])
    {
      forward_msg.data = true;
    }

    turbo_msg.data = false;
    if(_cmd_functions[CarFunction::TURBO])
    {
      turbo_msg.data = true;
    }

    back_msg.data = false;
    if(_cmd_functions[CarFunction::BACK])
    {
      back_msg.data = true;
    }

    _turn_publisher.publish(turn_msg);
    _forward_publisher.publish(forward_msg);
    _turbo_publisher.publish(turbo_msg);
    _backward_publisher.publish(back_msg);
    // _f1_publisher.publish(f1_msg);
    // _f2_publisher.publish(f2_msg);
  }

  void KeyboardControlInput::InitDisplay()
  {
    initscr();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    timeout(0);

    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);

    getmaxyx(stdscr, _row, _col);

// DBO
    printw("Screen size: %dx%d", _col, _row);
    refresh();
    usleep(5000000); // 5s
//

    clear();
    PrintDisplay();
    refresh();
  }

  void KeyboardControlInput::PrintDisplay()
  {
    // Print title and basic instructions
    std::string title("Scorpion RC Car Keyboard Input Controller");
    mvprintw(1, (_col-title.length())/2, title.c_str());
    mvprintw(3, 0, "Use the arrow keys to control the robot.");
    mvprintw(4, 0, "Press SHFT to active turbo.");

    // Print list of car functions
    std::string temp_text("Car Funtions (Active in ");
    mvprintw(6, 0, temp_text.c_str());
    attron(COLOR_PAIR(1));
    attron(A_BOLD);
    printw("Red");
    attroff(COLOR_PAIR(1));
    attroff(A_BOLD);
    printw(")");

    if(_active_functions[CarFunction::LEFT])
    {
      attron(COLOR_PAIR(1));
      attron(A_BOLD);
    }
    else
    {
      attroff(COLOR_PAIR(1));
      attroff(A_BOLD);
    }
    mvprintw(7, 3, "LEFT");

    if(_active_functions[CarFunction::RIGHT])
    {
      attron(COLOR_PAIR(1));
      attron(A_BOLD);
    }
    else
    {
      attroff(COLOR_PAIR(1));
      attroff(A_BOLD);
    }
    mvprintw(8, 3, "RIGHT");

    if(_active_functions[CarFunction::FORWARD])
    {
      attron(COLOR_PAIR(1));
      attron(A_BOLD);
    }
    else
    {
      attroff(COLOR_PAIR(1));
      attroff(A_BOLD);
    }
    mvprintw(9, 3, "FORWARD");

    if(_active_functions[CarFunction::TURBO])
    {
      attron(COLOR_PAIR(1));
      attron(A_BOLD);
    }
    else
    {
      attroff(COLOR_PAIR(1));
      attroff(A_BOLD);
    }
    mvprintw(10, 3, "TURBO");

    if(_active_functions[CarFunction::BACK])
    {
      attron(COLOR_PAIR(1));
      attron(A_BOLD);
    }
    else
    {
      attroff(COLOR_PAIR(1));
      attroff(A_BOLD);
    }
    mvprintw(11, 3, "BACK");

    if(_active_functions[CarFunction::F1])
    {
      attron(COLOR_PAIR(1));
      attron(A_BOLD);
    }
    else
    {
      attroff(COLOR_PAIR(1));
      attroff(A_BOLD);
    }
    mvprintw(12, 3, "F1");

    if(_active_functions[CarFunction::F2])
    {
      attron(COLOR_PAIR(1));
      attron(A_BOLD);
    }
    else
    {
      attroff(COLOR_PAIR(1));
      attroff(A_BOLD);
    }
    mvprintw(13, 3, "F2");

    // Print arrow keypad display
    temp_text = "Active arrow keys in ";
    mvprintw(6, (_col-temp_text.length())*3/4+(temp_text.length()/4), temp_text.c_str());
    attron(COLOR_PAIR(1));
    attron(A_BOLD);
    printw("Red");
    attroff(COLOR_PAIR(1));
    attroff(A_BOLD);

    // Up (Using unicode arrows would be nice)
    mvprintw(8, (_col*3/4)-1, "/\\");
    mvprintw(9, (_col*3/4)-1, "||");

    // Down
    mvprintw(11, (_col*3/4)-1, "||");
    mvprintw(12, (_col*3/4)-1, "\\/");

    // Right
    mvprintw(10, (_col*3/4)+1, "==>");

    // Left
    mvprintw(10, (_col*3/4)-4, "<==");
  }

  void KeyboardControlInput::TelemetryCallback(const scorpion_rc_msgs::CarTelemetry msg)
  {
    _active_functions[CarFunction::LEFT] = msg.active_functions[scorpion_rc_msgs::CarTelemetry::LEFT];
    _active_functions[CarFunction::RIGHT] = msg.active_functions[scorpion_rc_msgs::CarTelemetry::RIGHT];
    _active_functions[CarFunction::FORWARD] = msg.active_functions[scorpion_rc_msgs::CarTelemetry::FORWARD];
    _active_functions[CarFunction::TURBO] = msg.active_functions[scorpion_rc_msgs::CarTelemetry::TURBO];
    _active_functions[CarFunction::BACK] = msg.active_functions[scorpion_rc_msgs::CarTelemetry::BACK];
    _active_functions[CarFunction::F1] = msg.active_functions[scorpion_rc_msgs::CarTelemetry::F1];
    _active_functions[CarFunction::F2] = msg.active_functions[scorpion_rc_msgs::CarTelemetry::F2];
  }


} // end namespace scorpion_rc

int main(int argc, char** argv)
{
  ros::init(argc, argv, "keyboard_control_input");

  ros::NodeHandle nh;
  scorpion_rc::KeyboardControlInput keyboard_control(nh);
}
