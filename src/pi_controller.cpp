/**
 * \file   pi_controller.cpp
 * \brief  Brief description of file.
 * \Author James D. Burton (james.burton@vt.edu)
 * \date   September, 2016
 *
 * Description
 */

#include "scorpion_rc_control/pi_controller.h"

namespace scorpion_rc
{

// public
PiController::PiController(ros::NodeHandle& nh):
  _nh(nh),
  _update_rate(10),
  _telemetry_rate(10),
  _active_functions(),
  _function_pin_map(),
  _publishing_thread_prt(NULL),
  _microcontroller_interface_ptr(NULL),
  _loop_count(0),
  _telemetry_publisher(),
  _turn_subscriber(),
  _forward_subscriber(),
  _turbo_subscriber(),
  _backward_subscriber(),
  _f1_subscriber(),
  _f2_subscriber(),
  _desired_turn_angle(0),
  _desired_velocity(0),
  _mutex()
{
  init();
}

PiController::~PiController()
{
  _publishing_thread_prt->join();
}

void PiController::init()
{

  // Initialize function map
  _active_functions = {
    {scorpion_rc::CarFunction::LEFT, false},
    {scorpion_rc::CarFunction::RIGHT, false},
    {scorpion_rc::CarFunction::FORWARD, false},
    {scorpion_rc::CarFunction::TURBO, false},
    {scorpion_rc::CarFunction::BACK, false},
    {scorpion_rc::CarFunction::F1, false},
    {scorpion_rc::CarFunction::F2, false}
  };

  // Get controller update rate
  double update_rate;
  if(!_nh.getParam("update_rate", update_rate))
  {
    ROS_ERROR("Failed to get Pi controller update rate from parameter server!");
    ROS_ERROR("Using default rate of 10 Hz.");
    update_rate = 10;
  }
  _update_rate = ros::Rate(update_rate);

  // Get telemetry update rate
  double telemetry_rate;
  if(!_nh.getParam("update_rate", telemetry_rate))
  {
    ROS_ERROR("Failed to get Pi controller telemetry rate from parameter server!");
    ROS_ERROR("Using default rate of 10 Hz.");
    telemetry_rate = 10;
  }
  _telemetry_rate = ros::Rate(telemetry_rate);

  // Get car max velocity
  double max_vel;
  _nh.getParam("max_vel", max_vel);

  // Get GPIO pins by function
  std::map<std::string, int> function_gpio_map;
  _nh.getParam("function_gpio_pins", function_gpio_map);

  // Initialize function GPIO map
  _function_pin_map = {
    {scorpion_rc::CarFunction::LEFT, function_gpio_map["LEFT"]},
    {scorpion_rc::CarFunction::RIGHT, function_gpio_map["RIGHT"]},
    {scorpion_rc::CarFunction::FORWARD, function_gpio_map["FORWARD"]},
    {scorpion_rc::CarFunction::TURBO, function_gpio_map["TURBO"]},
    {scorpion_rc::CarFunction::BACK, function_gpio_map["BACK"]},
    {scorpion_rc::CarFunction::F1, function_gpio_map["F1"]},
    {scorpion_rc::CarFunction::F2, function_gpio_map["F2"]}
  };

  // Instantiate microcontroller interface
  _microcontroller_interface_ptr.reset(new CarMicrocontrollerInterface(_function_pin_map, _active_functions, max_vel));

  // Setup ROS publisher
  _telemetry_publisher = _nh.advertise<scorpion_rc_msgs::CarTelemetry>("telemetry", 10);

  // Setup ROS subscribers
  _turn_subscriber = _nh.subscribe("turn", 10, &PiController::TurnCallback, this);
  _forward_subscriber = _nh.subscribe("forward", 10, &PiController::ForwardCallback, this);
  _turbo_subscriber = _nh.subscribe("turbo", 10, &PiController::TurboCallback, this);
  _backward_subscriber = _nh.subscribe("back", 10, &PiController::BackwardCallback, this);
  _f1_subscriber = _nh.subscribe("f1", 10, &PiController::F1Callback, this);
  _f2_subscriber = _nh.subscribe("f2", 10, &PiController::F2Callback, this);


  _publishing_thread_prt.reset(new std::thread(&PiController::publishLoop, this));
}

void PiController::update()
{

  std::unique_lock<std::mutex> lock(_mutex, std::defer_lock);

  if(_microcontroller_interface_ptr)
  {
    if(lock.try_lock())
    {
      _microcontroller_interface_ptr->update(_active_functions, _desired_velocity);
      ++_loop_count;
    }
  }
  else
  {
    ROS_ERROR("_microcontroller_interface_ptr is NULL! Shutting down node!");
    ros::shutdown();
  }

  ros::spinOnce();
  _update_rate.sleep();
}

void PiController::publishLoop()
{
  while(ros::ok())
  {
    std::unique_lock<std::mutex> lock(_mutex, std::defer_lock);

    if(lock.try_lock())
    {
      PublishTelemetry();
    }
  }
}

void PiController::PublishTelemetry()
{
  scorpion_rc_msgs::CarTelemetry telemetry_msg;

  telemetry_msg.header.stamp = ros::Time::now();
  telemetry_msg.header.seq = _loop_count;

  for(std::map<scorpion_rc::CarFunction, bool>::iterator ii = _active_functions.begin(); ii != _active_functions.end(); ++ii)
  {
    size_t idx = std::distance(_active_functions.begin(), ii);
    telemetry_msg.active_functions[idx] = ii->second;
  }

  // Get steering motor angle and convert to wheel angle
  telemetry_msg.turn_angle = nan("");
  telemetry_msg.cmd_turn_angle = _desired_turn_angle;

  _telemetry_publisher.publish(telemetry_msg);
}

void PiController::TurnCallback(const ackermann_msgs::AckermannDriveStamped msg)
{
  if(msg.drive.steering_angle < 0)
  {
    _active_functions[scorpion_rc::CarFunction::RIGHT] = true;
    _active_functions[scorpion_rc::CarFunction::LEFT] = false;
  }
  else if(msg.drive.steering_angle > 0)
  {
    _active_functions[scorpion_rc::CarFunction::RIGHT] = false;
    _active_functions[scorpion_rc::CarFunction::LEFT] = true;
  }
  else
  {
    _active_functions[scorpion_rc::CarFunction::RIGHT] = false;
    _active_functions[scorpion_rc::CarFunction::LEFT] = false;
  }
}

void PiController::ForwardCallback(const std_msgs::Bool msg)
{
  if(msg.data)
  {
    _active_functions[scorpion_rc::CarFunction::FORWARD] = true;
  }
  else
  {
    _active_functions[scorpion_rc::CarFunction::FORWARD] = false;
  }
}

void PiController::TurboCallback(const std_msgs::Bool msg)
{
  if(msg.data && _active_functions[scorpion_rc::CarFunction::FORWARD]
     && !(_active_functions[scorpion_rc::CarFunction::LEFT] || _active_functions[scorpion_rc::CarFunction::RIGHT]))
  {
  _active_functions[scorpion_rc::CarFunction::TURBO] = true;
  }
  else
  {
  _active_functions[scorpion_rc::CarFunction::TURBO] = false;
  }
}

void PiController::BackwardCallback(const std_msgs::Bool msg)
{
  if(msg.data)
  {
    _active_functions[scorpion_rc::CarFunction::BACK] = true;
  }
  else
  {
    _active_functions[scorpion_rc::CarFunction::BACK] = false;
  }
}

void PiController::F1Callback(const std_msgs::Bool msg)
{
  if(msg.data)
  {
    _active_functions[scorpion_rc::CarFunction::F1] = true;
  }
  else
  {
    _active_functions[scorpion_rc::CarFunction::F1] = false;
  }
}

void PiController::F2Callback(const std_msgs::Bool msg)
{
  if(msg.data)
  {
    _active_functions[scorpion_rc::CarFunction::F2] = true;
  }
  else
  {
    _active_functions[scorpion_rc::CarFunction::F2] = false;
  }
}

} // end namespace scorpion_rc

int main(int argc, char** argv)
{
  ros::init(argc, argv, "pi_controller");

  ros::NodeHandle nh;
  scorpion_rc::PiController pi_controller(nh);

  while(ros::ok())
  {
    pi_controller.update();
  }

  return 0;
}
